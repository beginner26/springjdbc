package com.mkyong.controller;

import com.mkyong.model.Book;
import com.mkyong.repository.BookRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class RestApiController {

    public static final Logger logger = LoggerFactory.getLogger(RestApiController.class);

//    @Autowired
//    BookRepository bookRepository; //Service which will do all data retrieval/manipulation work

    @Autowired
    @Qualifier("jdbcBookRepository")
    private BookRepository bookRepository;

    // -------------------Create a Product-------------------------------------------
    @RequestMapping(value = "/product", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> createProduct(@RequestBody Book book){
        logger.info("Create Product {}", book);
        bookRepository.save(book);
        return new ResponseEntity<>(book, HttpStatus.CREATED);
    }

    // ------------------- Update a Product ------------------------------------------------
    @RequestMapping(value = "/product/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateProduct(@PathVariable("id") long id, @RequestBody Book book){
        logger.info("Update product with id {}", id);
        System.out.println(book.getName());

        Book currentBook = new Book();

        currentBook.setId(id);
        currentBook.setName(book.getName());
        currentBook.setPrice(book.getPrice());

        bookRepository.update(currentBook);
        return new ResponseEntity<>(currentBook, HttpStatus.OK);
    }

    // -------------------Retrieve Single Product------------------------------------------

    @RequestMapping(value = "/product/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getProduct(@PathVariable("id") long id){
    logger.info("Fetching book with id {}", id);

    Optional<Book> book = bookRepository.findById(id);

    return new ResponseEntity<>(book, HttpStatus.OK);
    }


    // -------------------Retrieve All Products--------------------------------------------
    @RequestMapping(value = "/product", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List<Book>> listAllProduct() {
        List<Book> books = bookRepository.findAll();
        if (books.isEmpty()) {
            return new ResponseEntity<>(books, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(books, HttpStatus.OK);
    }

    // ------------------- Delete a Product-----------------------------------------
    @RequestMapping(value = "/product/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteProduct(@PathVariable("id") long id){
        logger.info("Fetching & deleting product with id {}", id);

        bookRepository.deleteById(id);
        return new ResponseEntity<Book>(HttpStatus.NO_CONTENT);
    }



}
